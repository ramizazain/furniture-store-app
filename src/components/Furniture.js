import React, { useState } from 'react'
import './Furniture.css'

function Furniture() {
  const [furnitures,setFurnitures]= useState([
    {name: '3-Seater Leather Sofa', image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSZthboOds5Uz65hvFO8Rs4voZhLLsldlgj3A&usqp=CAU', price: '$1600'},
    {name: '2-Seater Sofa', image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRX4W0-3jsN9CRdvyF4qlTTXW2FkVSmIGowmD2CMj8ioUYFInyYjatWPcNLBhNCjk3lFVg&usqp=CAU', price: '$100'},
    {name: 'Pendent light', image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSYlpmL8JetjPEqttSTeteDd9tY4FEC_i8xeQ&usqp=CAU', price: '$345'},
    {name: 'Kids Study Table', image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1DVl9CPpjkPERIzQjYANZm6eHOtkLnkVYaA&usqp=CAU', price: '$400'},
    {name: 'Dining Table', image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTkMDxzSDJyFvaf6yyuy5c8vveG9_3__6xI5g&usqp=CAU', price: '$6000'},
    {name: 'Recliner Chair', image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcScWtJ7bFZ4Bx_3V0hQO5-YWD7BIKo_l0L2g98DKryec9ytGUOMlT3RPqqYgr-XSnDeLkc&usqp=CAU', price: '$8000'}

  ])
  return (
    <div className="furnitureListContainer">
      <div className="title">What People Love</div>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-around",
          backgroundColor: "#F5F5F5",
          marginTop: "20px",
          padding: "10px 0px 10px 0px",
        }}
      >
        {furnitures.map((item, index) => (
          <div className="furnitureItemContainer">
            <img src={item.image} className="furnitureImage" />
            <div className="nameText"> {item.name}</div>
          <div className='price'>{item.price}</div>
</div>
        ))}
      </div>
    </div>
  );
}

export default Furniture
