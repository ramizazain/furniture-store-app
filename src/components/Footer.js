import React from "react";
import "./Footer.css";
import {
  AiOutlineInstagram,
  AiFillFacebook,
  AiFillLinkedin,
} from "react-icons/ai";

function Footer() {
  return (
    <div className="footerContainer">
      <div className="footerLeft">
        <div className="footerItem">Explore</div>
        <div className="footerItem">About Us</div>
        <div className="footerItem">Contact Us</div>
        <div className="footerItem">Help</div>
        <div className="footerBottom">
          <div className="contactContainer">
            <div className="email">topfurniture@gmail.com</div>
            <div className="phone">+56734890</div>
          </div>
          <div className="locationContainer">
            Suntec City Mall,
            <br />
            East Wing,#02-7823467
          </div>
        </div>
      </div>
      <div className="footerRight">
        <div className="phone">Follow Us:</div>
        <AiOutlineInstagram
          size={10}
          color={"#2B495E"}
          className="iconContainer"
        />

        <AiFillFacebook size={10} color={"#2B495E"} className="iconContainer" />
        <AiFillLinkedin size={10} color={"#2B495E"} className="iconContainer" />
      </div>
    </div>
  );
}

export default Footer;
