import React, { useState } from "react";
import "./RoomList.css";

function RoomList() {
  const [rooms, setRooms] = useState([
    {
      image:
        "https://storage.googleapis.com/static_images/shop_product_images/sprint9/tmp/tmp_images/LSPS001/lifestyle/external_thumb/item_image_lifestyle_LSPS001_2.png",
      name: "Living Room",
    },
    {
      image:
        "https://images.unsplash.com/photo-1616594039964-ae9021a400a0?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8YmVkcm9vbSUyMGludGVyaW9yfGVufDB8fDB8fA%3D%3D&w=1000&q=80",
      name: "Bed Room",
    },
    {
      image:
        "https://royaloakindia.com/media/catalog/product/cache/bf9e4acdfb1328f7a363d76de02b979f/r/o/royaloak-milan-italian-marble-6-seater-dining-table-set-1.jpg",
      name: "Dining",
    },
    {
      image:
        "https://assets.myntassets.com/dpr_1.5,q_60,w_400,c_limit,fl_progressive/assets/images/9674233/2021/10/28/69eceaa2-4352-4a32-b79c-0de82e0bfa2e1635400820910HomesakeTransparentSolidHandcrafted3-LightsClusterGlassHangi1.jpg",
      name: "Lights",
    },
  ]);
  return (
    <div className="roomListContainer">
      <div className="title">Furnitures</div>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-around",
          backgroundColor: "#F5F5F5",
          marginTop: "20px",
          padding: "10px 0px 10px 0px",
        }}
      >
        {rooms.map((item, index) => (
          <div className="roomItemContainer">
            <img src={item.image} className="roomImage" />
            <div className="nameText"> {item.name}</div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default RoomList;
