import React from 'react'
import './HeroBanner.css'

function HeroBanner() {
  return (
    <div className='BannerContainer'>
        <div className='leftContainer'>
            <div className='storeTitle'>
            Your Place Will Never Be the Same Again with Our<br/> Fantastic Furniture.
            </div>
            <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
                <img className='sofaImage' src='https://i.pinimg.com/originals/3e/06/3d/3e063dbea335dd01a2b9f08e8483e221.png'/>
                <span className='button'>Shop Now</span>
            </div>
        </div>
        <div className='rightContainer'>
            <div className='offerSale'>August<br/>Sale</div>
        </div>
      
    </div>
  )
}

export default HeroBanner
