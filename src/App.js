import './App.css';
import Footer from './components/Footer';
import Furniture from './components/Furniture';
import HeroBanner from './components/HeroBanner';
import NavBar from './components/NavBar';
import RoomList from './components/RoomList';

function App() {
  return (
    <div className="App">
      <NavBar />
      <HeroBanner />
      <RoomList />
      <Furniture />
      <Footer />
    </div>
  );
}

export default App;
